# 天翼SDK接口说明

 - 已上传部分源代码，以供参考

### 天翼接口地址
paysdk.tyread.com:9417

### 天翼提交方式
POST

### 合作方接口地址

地址：60.205.140.88 端口：8999

### 合作方提交方式

socket 连接，二进制传输

### 大体流程

1. APP 启动的请求一次
2. 每次扣费时请求一次
(*) 3. 解密请求可根据需要进行请求

### 与合作方服务器传输的包结构

|  4字节  |  json  |

1. 4字节 : 指明 json 长度
2. json : 整个json数据体，传参都在json里

- 发送和服务器返回都遵从此格式

### 每次都要传的参数！！！

| 参数        | 示例   |  说明  |
| --------   | -----  | ----  |
| gameversion   | 1.0.9  | 游戏版本号  |
| gameversioncode   | 10009  | 这应该是天翼生成的版本号，开发者应该有  |
| gamepackagename   | com.htmd.package  | 包名  |
| gamename   | 毁天灭地  | 游戏名称  |
| ua   | HUAWEI C8815  | 手机型号  |
| connecttype   | wifi  | 网络环境，具体赋值参看【代码参考】->【截图】  |
| screenwh   | 540_960  | 分辨率（格式要一致）  |
| channel   | DAxxxx  | 天翼提供的渠道号  |
| imei   | A000xxxxxxxxD4A  | ----  |
| imsi   | 4600xxxxxxxx746  | ----  |
| udid   | 35ea5xxxxxxxxfb0  | 这个就是设备的 Device Id  |
| appid   | 08177xxxxxxxxxxxxxxxxxxxxxx24f9  | ----  |
| appkey   | 08177xxxxxxxxxxxxxxxxxxxxx24f9  | ----  |
| app_secret   | a00818xxxxxxxxxxxxxxxxxxx12f179  | 天翼提供的开发者 secret  |

### APP 启动时请求

- send

| 参数        | 示例   |  说明  |
| --------   | -----  | ----  |
| dmtytype   | DMTYType.queryStart  | 行为说明  |

- recv

| 参数        | 示例   |  说明  |
| --------   | -----  | ----  |
| action   | start  | 行为说明，APP start 的返回  |
| packages   | JSONArray  | 包含了需要请求的包  |

- packages 说明

| 参数        | 示例   |  说明  |
| --------   | -----  | ----  |
| method   | url_post  | - [url_post] 指以URL POST的方式将  content 内容提交给天翼指定的url <br/>  - [sms] 指将content 内容以短信的形式发给 toNum 指定的号码完成扣费 |
| url   | /pay-sdk/queryNewPayChannelV200  | 需要请求的URL  |
| when   | 0  | 请求时机说明，如果是数字，代表延迟发送的毫秒数 <br/>  如果是字串则按照字串说明操作 |
| index   | 1  | 发送顺序，从 1 开始计数  |
| content   | lu3FfU4iOX6RD0oOPHX……  | 需要发送的加密字串  |

### 付费时请求

- send

| 参数        | 示例   |  说明  |
| --------   | -----  | ----  |
| dmtytype   | DMTYType.queryRequest  | 行为说明  |
| orderid   | SMSOrderIDGenerator.getOrderID(15)  | 直接调用此方法就行，固定的  |
| itemid   | 0100  | 收费项目的 itemid 在  DMTYType.queryStart  返回的数据里有需要先Decode |
| paycode   | 4#xxxx#0  | 支付代码，从运营商处获取  |
| channelid   | DAxxxx  | 天翼提供的渠道号,  这里需要再提供一次  |
| userdata   | 134xxx  | 注册在天翼那边的订单号，开发者应该有，对应的应该就是 tradeId  |

- recv

| 参数        | 示例   |  说明  |
| --------   | -----  | ----  |
| action   | start  | 行为说明，APP start 的返回  |
| packages   | JSONArray  | 包含了需要请求的包  |

### Decode 请求
- Decode 不需要加上面【每次都要传的参数！！！】的参数
- send

| 参数        | 示例   |  说明  |
| --------   | -----  | ----  |
| dmtytype   | DMTYType.queryDecode  | ----  |
| encode   | gCuJr7tlu3FfU4iOX6RD0oOPHX51m/V5G1……  | ----  |

- recv

| 参数        | 示例   |  说明  |
| --------   | -----  | ----  |
| action   | decode | 行为说明  |
| decode   | {"action":"decode","content":"{\"order ……  | ----  |





