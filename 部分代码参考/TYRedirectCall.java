package dm.mina.server;

import java.nio.ByteBuffer;

import org.dm.server.translatemsg.DMTYType;
import org.json.JSONObject;

import com.tyread.platform.util.SMSOrderIDGenerator;

public class TYRedirectCall {
	static public ByteBuffer get_start() {
		JSONObject json = getqueryShared();
		
		json.put("dmtytype", DMTYType.queryStart);
		
		/*
		 * String.length()返回字符串的字符个数，一个中文算一个字符；
		 * String.getBytes().length 返回字符串的字节长度，一个中文两个字节；
		 * */
		byte[] bytes = json.toString().getBytes();
		ByteBuffer res = ByteBuffer.allocate(4 + bytes.length);
		try {
			res.putInt(bytes.length);
			res.put(ByteBuffer.wrap(bytes));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return res;
	}
	
	static public ByteBuffer get_Request() {
		JSONObject json = getqueryShared();
		
		json.put("dmtytype", DMTYType.queryRequest);
		json.put("orderid", SMSOrderIDGenerator.getOrderID(15));
		json.put("paycode", "4#xxxxx#0");
		json.put("itemid", "0100");
		json.put("channelid", "DAxxxx");
		json.put("userdata", "134xxx");	// 就是 tradeId
		
		
		/*
		 * String.length()返回字符串的字符个数，一个中文算一个字符；
		 * String.getBytes().length 返回字符串的字节长度，一个中文两个字节；
		 * */
		byte[] bytes = json.toString().getBytes();
		ByteBuffer res = ByteBuffer.allocate(4 + bytes.length);
		try {
			res.putInt(bytes.length);
			res.put(ByteBuffer.wrap(bytes));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return res;
	}
	
	static public ByteBuffer get_decode() {
		JSONObject json = new JSONObject();
		json.put("dmtytype", DMTYType.queryDecode);
		json.put("encode", "gCuJr7tlu3FfU4iOX6RD0oOPHX51m/V5G1J7JLNEfUBjn8owxM/VBk/+JGMCRZhBcvp1lWBtRAgzFoyj1k1bkkS6RTuSE9ctbP9X72SGI/e98EvUZlL7RkhGmtbl7XWD");
		
		byte[] bytes = json.toString().getBytes();
		ByteBuffer res = ByteBuffer.allocate(4 + bytes.length);
		res.putInt(bytes.length);
		res.put(ByteBuffer.wrap(bytes));
		
		return res;
	}
	
	static private JSONObject getqueryShared() {
		JSONObject json = new JSONObject();
		
        json.put("gameversion", "1.0.9");
        json.put("gameversioncode", "10009");
        json.put("gamepackagename", "com.htmd.package");
        json.put("gamename", "毁天灭地");
        json.put("ua", "HUAWEI C8815");
        json.put("connecttype", "wifi");
        json.put("screenwh", "540_960");
        json.put("channel", "DAxxxx");
        json.put("imei", "A000xxxxxxxD4A");
        json.put("imsi", "4600xxxxxxxxx46");
        json.put("udid", "35eaxxxxxxxx2fb0");	// device id
        json.put("appid", "08177cxxxxxxxxxxxxxxxxxxxx7d24f9");
        json.put("appkey", "08177cxxxxxxxxxxxxxxxxxxxx7d24f9");
        json.put("app_secret", "a008185xxxxxxxxxxxxxxxx8312f179");
		
		return json ;
	}
}
