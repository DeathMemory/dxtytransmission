package com.tyread.platform.util;

import java.util.Random;

public class SMSOrderIDGenerator {
    private static final int maxNum = 62;
    private static char[] str;

    static {
        SMSOrderIDGenerator.str = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    }

    public SMSOrderIDGenerator() {
        super();
    }

    public static String getOrderID(int oIdLen) {  // 0x0F
        String v0;
        for(v0 = SMSOrderIDGenerator.getRandomOrder(oIdLen); v0.toLowerCase().contains("null"); v0 = SMSOrderIDGenerator.getRandomOrder(oIdLen)) {
        }

        return v0;
    }

    private static String getRandomOrder(int len) {
        int v0 = 0;
        StringBuffer v2 = new StringBuffer("");
        Random rnd = new Random();
        while(v0 < len) {
            int v1 = Math.abs(rnd.nextInt(62));
            if(v1 < 0) {
                continue;
            }

            if(v1 >= SMSOrderIDGenerator.str.length) {
                continue;
            }

            v2.append(SMSOrderIDGenerator.str[v1]);
            ++v0;
        }

        return v2.toString();
    }
}

